import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-assignment',
  templateUrl: './assignment.component.html',
  styleUrls: ['./assignment.component.scss']
})
export class AssignmentComponent implements OnInit {

  constructor() { }

  room:number = 1;
  adult:number = 1;
  child:number = 0;
  remaining:number;
  roomcapacity:number;
  roomDes:boolean = false;
  adultDes:boolean = true;
  childDes:boolean = true;
  roomdcDes:boolean = true;
  adultInDes:boolean = false;
  childInDes:boolean = false;
  
  incrementRoom()
  {
    if((this.room>=1 && this.room<5))
    {
      
      if(this.room < (this.adult+this.child))
      {
        this.room += 1;
        if(this.room > (this.adult+this.child))
        {
          this.remaining = this.room - (this.adult+this.child)
          this.adult += this.remaining
          this.remaining = 0
        }
      }
      else if(this.room == (this.adult + this.child)) {
        this.room += 1;
        this.adult += 1;
      }
      if(this.room > 1) {
        this.roomdcDes = false;
      }
      if(this.adult > 1) {
        this.adultDes = false;
      }
    }
    if(this.room == 5)
    {
      this.roomDes = true;
    }
  }
  
  incrementAdult()
  {
    if(this.adult + this.child < 20) {
      this.adult += 1;
    }
    if((this.adult + this.child) == 20) {
      this.adultInDes = true;
      this.adultDes = false;
      this.childInDes = true;
      
    }
    
    
    if((this.adult + this.child) % 4 == 1)
    {
      if(this.room*4 <= (this.adult + this.child))
      {
      if(this.room < 5) {
        this.room +=1;
        if(this.room == 5) {
          this.roomDes = true;
          this.roomdcDes = false;
        }

      }
      
      }
    }
    if(this.adult > 1) {
      this.adultDes = false;
    }
  }
  
  incrementChild()
  {
    if(this.adult + this.child < 20) {
      this.child += 1;
      this.childDes = false;
    }
    if((this.adult + this.child) == 20) {
      this.childInDes = true;
      this.childDes = false;
      this.adultInDes = true;
      
    }
   
  
    if((this.adult + this.child) % 4 == 1)
    {
      if(this.room*4 <= (this.adult + this.child))
      {
        if(this.room < 5) {
          this.room +=1;
          if(this.room == 5) {
            this.roomDes = true;
            this.roomdcDes = false;
          }
        }
        
      }
    }
    if(this.child > 1) {
      this.childDes = false;
    }
    
  }
  
  
  decrementAdult()
  {
    if(this.adult != 1)
    {
      this.adult -= 1;
      if((this.adult + this.child) % 4 == 0)
      {
        this.room -=1;
      }
    }
    if(this.adult==1)
    {
      this.adultDes = true;
    }
    if((this.adult + this.child) < 20) {
      this.adultInDes = false;
      this.childInDes = false;
      
      
    } 
    
  }
  
  decrementChild()
  {
    if(this.child != 0)
    {
      this.child -= 1;
  
      if((this.adult + this.child) % 4 == 0)
      {
        this.room -=1;
      }
    }
    if(this.child==0)
    {
      this.childDes = true;
    }
    if((this.adult + this.child) < 20) {
      this.adultInDes = false;
      this.childInDes = false;
      
      
    } 
  }
  
  decrementRoom()
  {
    if(this.room != 1)
    {
      this.room -= 1;
      this.roomcapacity = this.room * 4
      if(this.roomcapacity < (this.adult+this.child))
      {
        this.remaining = (this.adult+this.child) - this.roomcapacity
        if(this.remaining >= this.child)
        {
          this.remaining = this.remaining - this.child
          this.child = 0
          this.adult = this.adult - this.remaining
          this.remaining = 0
        }
        else
        {
          this.child = this.child - this.remaining
          this.remaining = 0
        }
        
      }
      if(this.room < 5) {
        this.roomDes = false;
      }
      if(this.room == 1) {
        this.roomdcDes = true;
      }
      if(this.adult == 1) {
        this.adultDes = true;
      }
      if(this.child == 0) {
        this.childDes = true;
      }
    }
    else
    {
      this.roomdcDes = true;
    }
  }


  ngOnInit() {
   
  }

}